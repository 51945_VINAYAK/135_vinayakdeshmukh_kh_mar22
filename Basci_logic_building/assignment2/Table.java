//7. Write a Java program that takes a number as input and prints its multiplication table upto 10.
//Test Data:
//Input a number: 8
//Expected Output :
//8 x 1 = 8
//8 x 2 = 16
//8 x 3 = 24
//...
//8 x 10 = 80
import java.util.Scanner;
class Table
{
public static void main(String args[])
{

System.out.println("enter the number from user:"); 

Scanner sc= new Scanner(System.in);
int num=sc.nextInt();
System.out.println("Table of num" +num);
for(int i=1;i<=10;i++)
{
  int table;
  table=num*i;
  System.out.println(num + " x " + i + " = " +table );
  
}

}


}
