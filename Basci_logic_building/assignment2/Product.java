//5. Write a Java program that takes two numbers as input and display the product of two numbers.
//Test Data:
//Input first number: 25
//Input second number: 5
//Expected Output :
//25 x 5 = 125
import java.util.Scanner;
class Product
{
 public static void main(String args[])
 {
 Scanner input = new Scanner(System.in);
 System.out.print("enter the first number : ");
 int num1= input.nextInt();
 
 System.out.println("=======================================");
 
 System.out.print("enter the second number : ");
 int num2= input.nextInt();
 
 System.out.println("=======================================");
 
 int product= num1*num2;
 System.out.println("product of two number is  : "+product);
 
 }

}